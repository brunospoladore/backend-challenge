<?php

namespace App\Api\V1\Product;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use InfyOm\Generator\Request\APIRequest;

class CreateProductRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = $this->request->all();
        $sku = $request['sku'];
        $name = $request['name'];
        $customRules = Product::$rules;
        $customRules['sku'] = [
            'required',
            Rule::unique('products')->where(function ($query) use ($sku, $name) {
                return $query->where('sku', $sku)
                    ->where('name', $name);
            }),
        ];
        return $customRules;
    }

    public function messages()
    {
        $customMessages = parent::messages();
        $customMessages['sku.unique'] = 'Os campos SKU e NAME não podem se repetir.';
        return $customMessages;
    }

    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        $json = [
            'success' => false,
            'error' => $validator->errors()
        ];
        throw new HttpResponseException(response()->json($json, 422));
    }
}
