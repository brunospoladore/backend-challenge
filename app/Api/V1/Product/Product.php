<?php

namespace App\Api\V1\Product;

use App\Api\V1\OrderItem\OrderItem;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @version September 9, 2019, 1:48 am -03
 *
 * @property Collection orderItems
 * @property string sku
 * @property string name
 * @property float price
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'sku',
        'name',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sku' => 'string',
        'name' => 'string',
        'price' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'sku' => 'required',
        'name' => 'required',
        'price' => 'required|numeric|min:0|not_in:0'
    ];

    /**
     * @return HasMany
     **/
    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'product_id');
    }
}
