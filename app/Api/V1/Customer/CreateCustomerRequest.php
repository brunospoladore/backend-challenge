<?php

namespace App\Api\V1\Customer;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use InfyOm\Generator\Request\APIRequest;

class CreateCustomerRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = $this->request->all();
        $cpf = $request['cpf'];
        $email = $request['email'];
        $customRules = Customer::$rules;
        $customRules['cpf'] = [
            'required',
            'cpf',
            Rule::unique('customers')->where(function ($query) use ($cpf, $email) {
                return $query->where('cpf', $cpf)
                    ->where('email', $email);
            }),
        ];
        return $customRules;
    }

    public function messages()
    {
        $customMessages = parent::messages();
        $customMessages['cpf.unique'] = 'Os campos CPF e E-MAIL não podem se repetir.';
        return $customMessages;
    }

    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        $json = [
            'status' => false,
            'error' => $validator->errors()
        ];
        throw new HttpResponseException(response()->json($json, 422));
    }
}
