<?php

namespace App\Api\V1\Customer;

use App\Api\V1\Order\Order;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Customer
 * @package App\Models
 * @version September 9, 2019, 11:42 pm -03
 *
 */
class Customer extends Model
{
    use SoftDeletes;

    public $table = 'customers';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'cpf',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'cpf' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'cpf' => 'required|cpf',
        'email' => 'required|email'
    ];

    public function order()
    {
        return $this->hasMany(Order::class, 'customer_id');
    }

}
