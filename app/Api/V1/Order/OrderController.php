<?php

namespace App\Api\V1\Order;

use App\Api\V1\Customer\CustomerRepository;
use App\Api\V1\OrderItem\OrderItem;
use App\Api\V1\Product\ProductRepository;
use App\Http\Controllers\AppBaseController;
use DB;
use Exception;
use Response;

/**
 * Class OrderController
 * @package App\Http\Controllers\API
 */
class OrderController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;

    /** @var CustomerRepository */
    private $customerRepository;

    /** @var ProductRepository */
    private $productRepository;

    /** @var OrderItem */
    private $orderItem;

    /** @var Order */
    private $order;

    public function __construct(
        OrderRepository $orderRepo,
        Order $order,
        CustomerRepository $customerRepo,
        OrderItem $orderItem,
        ProductRepository $productRepository
    ) {
        $this->orderRepository = $orderRepo;
        $this->order = $order;
        $this->customerRepository = $customerRepo;
        $this->orderItem = $orderItem;
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the Order.
     * GET|HEAD /orders
     *
     * @return Response
     */
    public function index()
    {
        $orders = $this->order::with(['buyer', 'items', 'items.product'])->get();

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    /**
     * Store a newly created Order in storage.
     * POST /orders
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     * @throws Exception
     */
    public function store(CreateOrderRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $customer = $this->customerRepository->find($input['customer_id']);

            if ($customer === null) {
                throw new Exception('Customer not found', 404);
            }

            $order = $this->orderRepository->create($input);

            $totalOrder = 0;
            if (is_array($input['items']) && count($input['items']) > 0) {
                foreach ($input['items'] as $item) {
                    $product = $this->productRepository->find($item['product_id']);

                    if ($product === null) {
                        throw new Exception('Product not found', 404);
                    }

                    $totalItem = ($item['amount'] * $product->price);
                    $totalOrder += $totalItem;
                    $item['order_id'] = $order->id;
                    $item['price_unit'] = $product->price;
                    $item['total'] = $totalItem;
                    $this->orderItem::create($item);
                }
            } else {
                throw new Exception('É necessário pelo menos um item para realizar pedido.', 400);
            }

            $order = $this->orderRepository->find($order->id)->with(['buyer', 'items', 'items.product'])->first();
            DB::commit();
            return $this->sendResponse($order, 'Order saved successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return $this->sendError($e->getMessage());
        }
    }

    /**
     * Display the specified Order.
     * GET|HEAD /orders/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        return $this->sendResponse($order->toArray(), 'Order retrieved successfully');
    }

    /**
     * Update the specified Order in storage.
     * PUT/PATCH /orders/{id}
     *
     * @param int $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderRequest $request)
    {
        $input = $request->all();

        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        $order = $this->orderRepository->update($input, $id);

        return $this->sendResponse($order->toArray(), 'Order updated successfully');
    }

    /**
     * Remove the specified Order from storage.
     * DELETE /orders/{id}
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        $order->delete();

        return $this->sendResponse($id, 'Order deleted successfully');
    }
}
