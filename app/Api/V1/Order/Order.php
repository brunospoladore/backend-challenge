<?php

namespace App\Api\V1\Order;

use App\Api\V1\Customer\Customer;
use App\Api\V1\OrderItem\OrderItem;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Order
 * @package App\Models
 * @version September 9, 2019, 1:53 am -03
 *
 * @property Customer customer
 * @property Collection orderItems
 * @property integer customer_id
 * @property float total
 * @property string status
 */
class Order extends Model
{
    public $table = 'orders';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'customer_id',
        'total',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'total' => 'float',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_id' => 'required',
        'total' => 'required|numeric|min:0|not_in:0',
        'status' => 'required'
    ];

    /**
     * @return BelongsTo
     **/
    public function buyer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    /**
     * @return HasMany
     **/
    public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id');
    }
}
