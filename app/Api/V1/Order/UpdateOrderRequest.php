<?php

namespace App\Api\V1\Order;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use InfyOm\Generator\Request\APIRequest;

class UpdateOrderRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $customUpdateRules = [
            'status' => 'required'
        ];
        return $customUpdateRules;
    }

    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        $json = [
            'status' => false,
            'error' => $validator->errors()
        ];
        throw new HttpResponseException(response()->json($json, 422));
    }
}
