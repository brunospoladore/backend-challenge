<?php

namespace App\Api\V1\OrderItem;

use App\Api\V1\Order\Order;
use App\Api\V1\Product\Product;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderItem
 * @package App\Models
 * @version September 9, 2019, 11:45 pm -03
 *
 * @property Order order
 * @property Product product
 * @property integer order_id
 * @property integer product_id
 * @property integer amount
 * @property float price_unit
 * @property float total
 */
class OrderItem extends Model
{
    public $table = 'order_items';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'order_id',
        'product_id',
        'amount',
        'price_unit',
        'total'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'product_id' => 'integer',
        'amount' => 'integer',
        'price_unit' => 'float',
        'total' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'order_id' => 'required',
        'product_id' => 'required',
        'amount' => 'required|integer|min:0|not_in:0',
        'price_unit' => 'required|numeric|min:0|not_in:0',
        'total' => 'required|numeric|min:0|not_in:0'
    ];

    /**
     * @return BelongsTo
     **/
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
