# < back >Developer</ end >

## Requerimentos
- **PHP >= 7.1.3**
- **Composer**
- **MySQL Database**

## Instalação
```bash
git clone https://brunospoladore@bitbucket.org/brunospoladore/backend-challenge.git
composer install
```
**SQL Script**
```sql
CREATE DATABASE `backend_challenge` CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';
```

## Configuração

```php
php artisan migrate
php artisan db:seed
```

## Como Funciona

#### 1 - Inicie o servidor
```
php artisan serve
```

#### 2 - Crie um  usuário
### POST `/api/v1/auth/signup`
| Campo       | Tipo     |
|-------------|----------|
| name        | string   |
| email       | string   |
| password    | string   |

#### 3 - Faça login com o novo usuário
### POST `/api/v1/auth/login`
| Campo       | Tipo     |
|-------------|----------|
| email       | string   |
| password    | string   |

- O sistema retornará o token de autenticação. Acrescente-o no header da requisição para as funcionalidades de regras de negócio.
```
Authorization: Bearer {token}
``` 

#### 4 - Demais requisições

### POST `/api/v1/products`
### GET `/api/v1/products`
### POST `/api/v1/customers`
### POST `/api/v1/orders`
### PUT `/api/v1/orders/{{ID_ORDER}}`
### GET `/api/v1/orders`