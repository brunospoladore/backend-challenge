<?php

use App\Api\V1\Order\Order;
use App\Api\V1\OrderItem\OrderItem;
use App\Api\V1\Product\Product;
use Illuminate\Database\Seeder;

class OrderItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 80; $i++) {
            factory(OrderItem::class)->create([
                'order_id' => $this->getRandomOrderId(),
                'product_id' => $this->getRandomProductId()
            ]);
        }
    }

    private function getRandomOrderId()
    {
        $order = Order::inRandomOrder()->first();
        return $order->id;
    }

    private function getRandomProductId()
    {
        $product = Product::inRandomOrder()->first();
        return $product->id;
    }
}
