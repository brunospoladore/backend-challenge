<?php

use App\Api\V1\Customer\Customer;
use App\Api\V1\Order\Order;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 80; $i++) {
            factory(Order::class)->create([
                'customer_id' => $this->getRandomCustomerId()
            ]);
        }
    }

    private function getRandomCustomerId()
    {
        $customer = Customer::inRandomOrder()->first();
        return $customer->id;
    }
}
