<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrderItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->foreign('order_id', 'order_id_fk')->references('id')->on('orders')
                ->onUpdate('CASCADE')->onDelete('NO ACTION');
            $table->foreign('product_id', 'product_id_fk')->references('id')->on('products')
                ->onUpdate('CASCADE')->onDelete('NO ACTION');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropForeign('order_id_fk');
            $table->dropForeign('product_id_fk');
        });
    }

}
