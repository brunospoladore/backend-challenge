<?php

/** @var Factory $factory */

use App\Api\V1\Order\Order;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Order::class, function (Faker $faker) {
    $date = Carbon::now();
    return [
        'total' => $faker->randomFloat(2, 1, 999),
        'status' => $faker->word,
        'created_at' => $date,
        'updated_at' => $date
    ];
});
