<?php

/** @var Factory $factory */

use App\Api\V1\Customer\Customer;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Faker\Provider as FakerProvider;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Customer::class, function (Faker $faker) {
    $faker->addProvider(new FakerProvider\pt_BR\Person($faker));
    $date = Carbon::now();
    return [
        'name' => $faker->name,
        'cpf' => $faker->cpf,
        'email' => $faker->email,
        'created_at' => $date,
        'updated_at' => $date
    ];
});
