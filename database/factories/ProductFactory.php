<?php

/** @var Factory $factory */

use App\Api\V1\Product\Product;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Keygen\Keygen;

$factory->define(Product::class, function (Faker $faker) {
    $date = Carbon::now();
    return [
        'sku' => Keygen::numeric(16)->generate(),
        'name' => $faker->sentence(4),
        'price' => $faker->randomFloat(2, 1, 999),
        'created_at' => $date,
        'updated_at' => $date,
        'deleted_at' => null
    ];
});
