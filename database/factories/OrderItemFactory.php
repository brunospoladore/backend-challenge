<?php

/** @var Factory $factory */

use App\Api\V1\OrderItem\OrderItem;
use App\Api\V1\Product\Product;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(OrderItem::class, function (Faker $faker) {
    $product = Product::inRandomOrder()->first();
    $amount = $faker->numberBetween(1, 5);
    $date = Carbon::now();
    return [
        'amount' => $amount,
        'price_unit' => $product->price,
        'total' => ($amount * $product->price),
        'created_at' => $date,
        'updated_at' => $date
    ];
});
