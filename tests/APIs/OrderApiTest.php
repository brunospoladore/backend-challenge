<?php namespace Tests\APIs;

use App\Api\V1\Order\Order;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\ApiTestTrait;
use Tests\TestCase;

class OrderApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order()
    {
        $order = factory(Order::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/v1/orders', $order
        );

        $this->assertApiResponse($order);
    }

    /**
     * @test
     */
    public function test_read_order()
    {
        $order = factory(Order::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/v1/orders/' . $order->id
        );

        $this->assertApiResponse($order->toArray());
    }

    /**
     * @test
     */
    public function test_update_order()
    {
        $order = factory(Order::class)->create();
        $editedOrder = factory(Order::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/v1/orders/' . $order->id,
            $editedOrder
        );

        $this->assertApiResponse($editedOrder);
    }

    /**
     * @test
     */
    public function test_delete_order()
    {
        $order = factory(Order::class)->create();

        $this->response = $this->json(
            'DELETE',
            '/api/v1/orders/' . $order->id
        );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/v1/orders/' . $order->id
        );

        $this->response->assertStatus(404);
    }
}
