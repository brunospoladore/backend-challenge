<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'v1'], function (Router $api) {
        $api->group(['prefix' => 'auth'], function (Router $api) {
            $api->post('signup', 'App\Api\V1\Auth\Controllers\SignUpController@signUp');
            $api->post('login', 'App\Api\V1\Auth\Controllers\LoginController@login');

            $api->post('recovery', 'App\Api\V1\Auth\Controllers\ForgotPasswordController@sendResetEmail');
            $api->post('reset', 'App\Api\V1\Auth\Controllers\ResetPasswordController@resetPassword');

            $api->post('logout', 'App\Api\V1\Auth\Controllers\LogoutController@logout');
            $api->post('refresh', 'App\Api\V1\Auth\Controllers\RefreshController@refresh');
            $api->get('me', 'App\Api\V1\Auth\Controllers\UserController@me');
        });

        $api->group(['middleware' => 'jwt.auth'], function (Router $api) {
            $api->resource('customers', 'App\Api\V1\Customer\CustomerController');
            $api->resource('orders', 'App\Api\V1\Order\OrderController');
            $api->resource('order_items', 'App\Api\V1\OrderItem\OrderItemController');
            $api->resource('products', 'App\Api\V1\Product\ProductController');
        });
    });
});
